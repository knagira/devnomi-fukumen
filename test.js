//module("poker.core.getHandCategory");
test("easy example", function() {
  var problem = ["aa","b","bcc"];
  var result = eval_fukumen(problem);
  deepEqual(result, ["99","1","100"]);
});

test("send more money", function() {
  var problem = ["send","more","money"];
  var result = eval_fukumen(problem);
  deepEqual(result, ["9567","1085","10652"]);
});

test("forest", function() {
  var problem = ["woods","woods","woods","forest"];
  var result = eval_fukumen(problem);
  deepEqual(result, ["71156","71156","71156","213468"]);
});

test("fifty", function() {
  var problem = ["ten","ten","ten","one","one","nine","nine","fifty"];
  var result = eval_fukumen(problem);
  deepEqual(result, ["794","794","794","649","649","4249","4249","12178"]);
});

test("zero", function(){
  var problem = ["a","a","a"];
  var result = eval_fukumen(problem);
  deepEqual(result, ["0","0","0"]);
});
